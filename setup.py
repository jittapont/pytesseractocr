import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pytesseractocr",
    version="0.0.1",
    author="Jittapon Thamrongsawad",
    author_email="jittapon.tha@shopee.com",
    description="A simple wrapper for google tesseract ocr",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/jittapont/pytesseractocr",
    packages=["pytesseractocr"],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires=">=3.7",
    install_requires=["numpy", "opencv-python", "Pillow"],
)
