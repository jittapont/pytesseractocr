# Pytesseractocr
[![pipeline status](https://gitlab.com/jittapont/pytesseractocr/badges/master/pipeline.svg)](https://gitlab.com/jittapont/pytesseractocr/-/commits/master) [![coverage report](https://gitlab.com/jittapont/pytesseractocr/badges/master/coverage.svg)](https://gitlab.com/jittapont/pytesseractocr/-/commits/master)

> A simple wrapper for google tesseract ocr


Author
-------------
-  Jittapon Thamrongsawad (jittapon.t@gmail.com)


Small Example
----------------
```python
   
    import cv2

    from pytesseractocr.tesseract import Tesseract

    tesseract = Tesseract("/usr/bin/tesseract", "")

    image = cv2.imread("pytesseractocr/test_data/test_tess.png")

    # Produce text, location and bounding box of text
    data = tesseract.image_to_data(
        image,
        langs=["eng"],
        configs="",
    )
    print(data)

    # Read text from image
    text = tesseract.image_to_string(image, langs=["eng"])
    print(text)

```

How to test
----------------
> This library use **python:3.7.9-buster** as base image for testing

1. Clone this git repository
```bash
   git clone ...
```

2. Pull python image and copy library to docker container
```
    docker pull python:3.7.9-buster
```

3. Install dependencies from **requirements.txt** inside docker container
```bash
   pip install -r requirements.txt
   apt update -y
   apt install -y tesseract-ocr libgl1-mesa-glx
```

4. Run **pytest**
```bash
    pytest -svv --cov-report term-missing --cov=pytesseractocr pytesseractocr/
```