import pickle

import cv2

from pytesseractocr.tesseract import Tesseract

tesseract = Tesseract("/usr/bin/tesseract", "")

image = cv2.imread("pytesseractocr/test_data/test_tess.png")

with open("pytesseractocr/test_data/data.pkl", "rb") as pkl:
    expected_data = pickle.load(pkl)

with open("pytesseractocr/test_data/text_data.pkl", "rb") as pkl:
    expected_text_data = pickle.load(pkl)


def test_image_to_data():
    data = tesseract.image_to_data(
        image,
        langs=["eng"],
        configs="",
    )
    assert expected_data == data


def test_image_to_string():
    text = tesseract.image_to_string(image, langs=["eng"])
    assert text == expected_text_data
