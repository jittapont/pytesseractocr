import logging
import shlex
import subprocess
import tempfile
from distutils.version import LooseVersion
from functools import partial
from typing import List, Optional, Union

import numpy as np
from PIL import Image

from .exceptions import TesseractNotFoundError
from .models import ImageObject, SupportedImageFormats, TesseractOutput

logger = logging.getLogger(__name__)


class Tesseract:
    def __init__(self, tesseract_executable: str, tessdata_path: str = ""):
        self.tesseract_executable = tesseract_executable
        self.tessdata_path = tessdata_path

    @property
    def tesseract_executable(self) -> str:
        return self._tesseract_executable

    @tesseract_executable.setter
    def tesseract_executable(self, tesseract_executable: str) -> None:
        self._tesseract_executable = tesseract_executable
        try:
            subprocess.run([self._tesseract_executable, "--version"])
        except Exception as e:
            raise TesseractNotFoundError("Can't find tesseract inside path") from e

    @property
    def version(self) -> str:
        try:
            return LooseVersion(
                (
                    subprocess.run(
                        [self.tesseract_executable, "--version"],
                        check=True,
                        stderr=subprocess.PIPE,
                        stdout=subprocess.PIPE,
                    )
                    .stdout.decode("utf-8")
                    .split("\n")[0]
                    .split()[1]
                )
            ).vstring
        except OSError:
            raise TesseractNotFoundError("Can't find tesseract inside path")

    @staticmethod
    def _prepare_image(image: Union[Image.Image, np.ndarray]) -> ImageObject:
        if isinstance(image, np.ndarray):
            image = Image.fromarray(image)

        if not isinstance(image, Image.Image):
            raise TypeError("Unsupported image object")

        image_format = "PNG" if not image.format else image.format
        if image_format not in [e.value for e in SupportedImageFormats]:
            raise TypeError("Unsupported image format/type")

        if "A" in image.getbands():
            background = Image.new("RGB", image.size, (255, 255, 255))
            background.paste(image, (0, 0), image.getchannel("A"))
            image = background

        image.format = image_format
        return ImageObject(image=image, format=image_format)

    @staticmethod
    def _save_image(image: ImageObject, temp_image) -> str:
        temp_image_file_path = image.get_image_path(temp_image.name)
        image.image.save(temp_image_file_path, format=image.format)
        return temp_image_file_path

    @staticmethod
    def _get_additional_subporcess_args(
        *, stdout: int, stderr: int, check: bool, timeout: int = 0
    ):
        kwargs = {"stdout": stdout, "stderr": stderr, "check": check}
        if timeout > 0:
            kwargs["timeout"] = timeout
        return kwargs

    @staticmethod
    def _make_lang_string(langs: List[str]) -> str:
        if langs is not None:
            lang_string = "+".join(langs)
        else:
            lang_string = ""
        return lang_string

    def _create_command_line_args(
        self, temp_image_file: str, lang_string: str, config_string: str
    ) -> List[str]:
        cmd_args = [
            self.tesseract_executable,
            "--tessdata-dir",
            self.tessdata_path,
            temp_image_file,
            "stdout",
        ]
        if len(lang_string) > 0:
            cmd_args += ["-l", lang_string]
        if len(config_string) > 0:
            cmd_args.extend(shlex.split(config_string))
        return cmd_args

    def _run_tesseract(
        self,
        image: Union[Image.Image, np.ndarray],
        *,
        langs: Optional[List[str]] = None,
        configs: str = "",
        timeout: int = 0,
    ) -> str:
        temp_image_file = tempfile.NamedTemporaryFile()
        prepared_image = self._prepare_image(image)
        temp_image_file_path = self._save_image(prepared_image, temp_image_file)

        lang_string = self._make_lang_string(langs)
        cmd_args = self._create_command_line_args(
            temp_image_file_path, lang_string, configs
        )
        logger.debug(f"command line arguments : {cmd_args}")
        partial_subprocess_run = partial(subprocess.run, cmd_args)
        subporcess_args = self._get_additional_subporcess_args(
            stdout=subprocess.PIPE, stderr=subprocess.PIPE, check=True, timeout=timeout
        )
        logger.debug(f"additional subporcess args : {subporcess_args}")
        process = partial_subprocess_run(**subporcess_args)
        stdout = process.stdout.decode()
        logger.debug(f"stdout : {repr(stdout)}")
        logger.debug(f"stderr : {repr(process.stderr.decode())}")
        temp_image_file.close()
        return stdout

    def image_to_data(
        self,
        image: Union[Image.Image, np.ndarray],
        *,
        langs: Optional[List[str]] = None,
        configs: str = "",
        timeout: int = 0,
    ) -> List[TesseractOutput]:
        stdout = self._run_tesseract(
            image,
            langs=langs,
            configs=f"{configs} -c tessedit_create_tsv=1",
            timeout=timeout,
        )
        column_length = len(stdout.split("\n")[0].split("\t"))
        return [
            TesseractOutput.from_list(row.split("\t"))
            for row in stdout.split("\n")[1:]
            if len(row.split("\t")) == column_length
        ]

    def image_to_string(
        self,
        image: Union[Image.Image, np.ndarray],
        *,
        langs: Optional[List[str]] = None,
        configs: str = "",
        timeout: int = 0,
    ) -> str:
        return self._run_tesseract(image, langs=langs, configs=configs, timeout=timeout)
