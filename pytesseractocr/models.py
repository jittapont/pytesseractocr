from dataclasses import dataclass
from enum import Enum
from os import extsep

from PIL import Image


@dataclass(frozen=True)
class ImageObject:
    image: Image
    format: str

    def get_image_path(self, file_path: str) -> str:
        return f"{file_path}{extsep}{self.format}"


class SupportedImageFormats(Enum):
    JPEG = "JPEG"
    PNG = "PNG"
    PBM = "PBM"
    PGM = "PGM"
    PPM = "PPM"
    TIFF = "TIFF"
    BMP = "BMP"
    GIF = "GIF"
    WEBP = "WEBP"


@dataclass(frozen=True)
class BoundingBox:
    left: int
    top: int
    width: int
    height: int

    @classmethod
    def from_list(cls, bounding_box_list):
        return cls(*bounding_box_list)


@dataclass(frozen=True)
class TesseractOutput:
    level: int
    page_num: int
    block_num: int
    par_num: int
    line_num: int
    word_num: int
    bounding_box: BoundingBox
    conf: float
    text: str

    @classmethod
    def from_list(cls, output):
        return cls(
            output[0],
            output[1],
            output[2],
            output[3],
            output[4],
            output[5],
            BoundingBox.from_list(output[6:10]),
            output[10],
            output[11],
        )
